#include <stdio.h>

typedef void(*initMat) (struct Matrix*, unsigned int row, unsigned int col);
typedef void(*initVec) (struct Vector*, unsigned int size);
typedef void(*matmul) (struct Matrix *, struct Matrix *);
typedef void(*matadd)(struct Matrix *, struct Matrix *);
typedef void(*L1Norm)(struct Matrix *);

typedef struct Matrix{
	unsigned int row;
	unsigned int col;
	int ** vec;
	initMat init;
	matmul func;
	matadd func2;
	L1Norm func3;
} MbyNMatrix;

typedef struct Vector{
	MbyNMatrix M;
	initVec init;
} NdimVector;

void matrix_L1_Norm(struct Matrix *A){
	int sum = 0;
		for (int i = 0; i < A->row; i++){
			for (int j = 0; j < A->col; j++){
				sum += A->vec[i][j];
			}
		}
		printf("%d", sum);
}

void matrixMult(struct Matrix *A, struct Matrix *B)
{
	int ** Ans;
	Ans = malloc(A->row * sizeof(int *));
	for (int i = 0; i < A->row; i++){
		Ans[i] = malloc(B->col * sizeof(int *));
	}

	if (A->col == B->row){
		int sum;
		for (int i = 0; i < A->row; i++){
			for (int j = 0; j < B->col; j++){
				sum = 0;
				for (int k = 0; k < B->row; k++){
					sum += A->vec[i][k] * B->vec[k][j];
				}
				Ans[i][j] = sum;
			}
		}
	}

	for (int i = 0; i < A->row; i++){
		for (int j = 0; j < B->col; j++){
			printf("%d     ", Ans[i][j]);
		}
		printf("\n");
	}

}

void matrixAdd(struct Matrix *A, struct Matrix *B){
	if ((A->row == B->row) && (A->col == B->col)){
		for (int i = 0; i < A->row; i++){
			for (int j = 0; j < A->col; j++){
				printf("%d     ", A->vec[i][j] + B->vec[i][j]);
			}
			printf("\n");
		}

	}
}

void initMatrix(struct Matrix* M, unsigned int row, unsigned int col) {
	MbyNMatrix A;
	A.col = col;
	A.row = row;
	A.vec = malloc(row * sizeof(int *));
	A.func = matrixMult;
	A.func2 = matrixAdd;
	A.func3 = matrix_L1_Norm;

	for (int i = 0; i < row; i++){
		A.vec[i] = malloc(col * sizeof(int *));
	}

	for (int i = 0; i < row; i++){
		for (int j = 0; j < col; j++){
			A.vec[i][j] = 1;
		}
	}

	for (int i = 0; i < row; i++){
		for (int j = 0; j < col; j++){
			printf("%d     ", A.vec[i][j]);
		}
		printf("\n");
	}

	*M = A;
}

void initVector(struct Vector* V, unsigned int size) {
	NdimVector A;
	A.M.init = initMatrix;
	A.M.init(&A.M, size, 1);
	*V = A;
}

int main(){

	/************************* TASK 1 ****************************/
	printf("Matrix A\n");
	MbyNMatrix A;
	A.init = initMatrix;
	A.init(&A, 3, 3);

	printf("\nMatrix B\n");
	MbyNMatrix B;
	B.init = initMatrix;
	B.init(&B, 3, 3);

	printf("\nMatrix C = A * B\n");
	A.func(&A, &B);

	printf("\nMatrix D = A + B\n");
	A.func2(&A, &B);

	printf("\nMatrix A L1 Norm = ");
	A.func3(&A);

	printf("\n\nVector E\n");
	NdimVector E;
	E.init = initVector;
	E.init(&E, 3);

	printf("\nVector F\n");
	NdimVector F;
	F.init = initVector;
	F.init(&F, 3);
	
	printf("\nVector G = A * E\n");
	E.M.func(&A, &E.M);

	printf("\nVector H = E + F\n");
	E.M.func2(&E.M, &F.M);

	printf("\nVector E L1 Norm = ");
	E.M.func3(&E.M);
	
	getchar();
	return 0;
}


